﻿namespace DotNetCasProxyDemoApp
{
    partial class DotNetCasProxyDemoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProxyValidateUrlLabel = new System.Windows.Forms.Label();
            this.TargetServiceNameLabel = new System.Windows.Forms.Label();
            this.ProxyTicketLabel = new System.Windows.Forms.Label();
            this.TargetServiceNameField = new System.Windows.Forms.TextBox();
            this.ProxyTicketField = new System.Windows.Forms.TextBox();
            this.ProxyValidateUrl = new System.Windows.Forms.TextBox();
            this.ServerResponseField = new System.Windows.Forms.TextBox();
            this.ValidationUrlLabel = new System.Windows.Forms.Label();
            this.ValidationUrlField = new System.Windows.Forms.TextBox();
            this.StatusColorField = new System.Windows.Forms.Panel();
            this.StatusExceptionField = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.StatusField = new System.Windows.Forms.Label();
            this.UsernameLabel = new System.Windows.Forms.Label();
            this.ProxiesLabel = new System.Windows.Forms.Label();
            this.UsernameField = new System.Windows.Forms.TextBox();
            this.ProxiesField = new System.Windows.Forms.ListBox();
            this.MessageLabel = new System.Windows.Forms.Label();
            this.MessageField = new System.Windows.Forms.TextBox();
            this.StatusColorField.SuspendLayout();
            this.SuspendLayout();
            // 
            // ProxyValidateUrlLabel
            // 
            this.ProxyValidateUrlLabel.AutoSize = true;
            this.ProxyValidateUrlLabel.Location = new System.Drawing.Point(16, 25);
            this.ProxyValidateUrlLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ProxyValidateUrlLabel.Name = "ProxyValidateUrlLabel";
            this.ProxyValidateUrlLabel.Size = new System.Drawing.Size(134, 17);
            this.ProxyValidateUrlLabel.TabIndex = 0;
            this.ProxyValidateUrlLabel.Text = "Proxy Validate URL:";
            // 
            // TargetServiceNameLabel
            // 
            this.TargetServiceNameLabel.AutoSize = true;
            this.TargetServiceNameLabel.Location = new System.Drawing.Point(15, 63);
            this.TargetServiceNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.TargetServiceNameLabel.Name = "TargetServiceNameLabel";
            this.TargetServiceNameLabel.Size = new System.Drawing.Size(146, 17);
            this.TargetServiceNameLabel.TabIndex = 1;
            this.TargetServiceNameLabel.Text = "Target Service Name:";
            // 
            // ProxyTicketLabel
            // 
            this.ProxyTicketLabel.AutoSize = true;
            this.ProxyTicketLabel.Location = new System.Drawing.Point(15, 101);
            this.ProxyTicketLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ProxyTicketLabel.Name = "ProxyTicketLabel";
            this.ProxyTicketLabel.Size = new System.Drawing.Size(89, 17);
            this.ProxyTicketLabel.TabIndex = 2;
            this.ProxyTicketLabel.Text = "Proxy Ticket:";
            // 
            // TargetServiceNameField
            // 
            this.TargetServiceNameField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TargetServiceNameField.Location = new System.Drawing.Point(177, 59);
            this.TargetServiceNameField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TargetServiceNameField.Name = "TargetServiceNameField";
            this.TargetServiceNameField.Size = new System.Drawing.Size(824, 22);
            this.TargetServiceNameField.TabIndex = 3;
            // 
            // ProxyTicketField
            // 
            this.ProxyTicketField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProxyTicketField.Location = new System.Drawing.Point(177, 97);
            this.ProxyTicketField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProxyTicketField.Name = "ProxyTicketField";
            this.ProxyTicketField.Size = new System.Drawing.Size(824, 22);
            this.ProxyTicketField.TabIndex = 4;
            // 
            // ProxyValidateUrl
            // 
            this.ProxyValidateUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ProxyValidateUrl.Location = new System.Drawing.Point(177, 21);
            this.ProxyValidateUrl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProxyValidateUrl.Name = "ProxyValidateUrl";
            this.ProxyValidateUrl.Size = new System.Drawing.Size(824, 22);
            this.ProxyValidateUrl.TabIndex = 5;
            // 
            // ServerResponseField
            // 
            this.ServerResponseField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ServerResponseField.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ServerResponseField.Location = new System.Drawing.Point(20, 182);
            this.ServerResponseField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ServerResponseField.Multiline = true;
            this.ServerResponseField.Name = "ServerResponseField";
            this.ServerResponseField.Size = new System.Drawing.Size(981, 182);
            this.ServerResponseField.TabIndex = 6;
            // 
            // ValidationUrlLabel
            // 
            this.ValidationUrlLabel.AutoSize = true;
            this.ValidationUrlLabel.Location = new System.Drawing.Point(15, 139);
            this.ValidationUrlLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ValidationUrlLabel.Name = "ValidationUrlLabel";
            this.ValidationUrlLabel.Size = new System.Drawing.Size(106, 17);
            this.ValidationUrlLabel.TabIndex = 2;
            this.ValidationUrlLabel.Text = "Validation URL:";
            // 
            // ValidationUrlField
            // 
            this.ValidationUrlField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ValidationUrlField.Location = new System.Drawing.Point(177, 135);
            this.ValidationUrlField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ValidationUrlField.Name = "ValidationUrlField";
            this.ValidationUrlField.Size = new System.Drawing.Size(824, 22);
            this.ValidationUrlField.TabIndex = 4;
            // 
            // StatusColorField
            // 
            this.StatusColorField.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatusColorField.Controls.Add(this.StatusExceptionField);
            this.StatusColorField.Location = new System.Drawing.Point(177, 384);
            this.StatusColorField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.StatusColorField.Name = "StatusColorField";
            this.StatusColorField.Size = new System.Drawing.Size(26, 24);
            this.StatusColorField.TabIndex = 7;
            // 
            // StatusExceptionField
            // 
            this.StatusExceptionField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatusExceptionField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatusExceptionField.Location = new System.Drawing.Point(0, 0);
            this.StatusExceptionField.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusExceptionField.Name = "StatusExceptionField";
            this.StatusExceptionField.Size = new System.Drawing.Size(24, 22);
            this.StatusExceptionField.TabIndex = 0;
            this.StatusExceptionField.Text = "!";
            this.StatusExceptionField.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.StatusExceptionField.Visible = false;
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(28, 388);
            this.StatusLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(120, 17);
            this.StatusLabel.TabIndex = 2;
            this.StatusLabel.Text = "Response Status:";
            // 
            // StatusField
            // 
            this.StatusField.AutoSize = true;
            this.StatusField.Location = new System.Drawing.Point(212, 388);
            this.StatusField.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.StatusField.Name = "StatusField";
            this.StatusField.Size = new System.Drawing.Size(0, 17);
            this.StatusField.TabIndex = 8;
            // 
            // UsernameLabel
            // 
            this.UsernameLabel.AutoSize = true;
            this.UsernameLabel.Location = new System.Drawing.Point(28, 426);
            this.UsernameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.UsernameLabel.Name = "UsernameLabel";
            this.UsernameLabel.Size = new System.Drawing.Size(77, 17);
            this.UsernameLabel.TabIndex = 2;
            this.UsernameLabel.Text = "Username:";
            // 
            // ProxiesLabel
            // 
            this.ProxiesLabel.AutoSize = true;
            this.ProxiesLabel.Location = new System.Drawing.Point(423, 388);
            this.ProxiesLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ProxiesLabel.Name = "ProxiesLabel";
            this.ProxiesLabel.Size = new System.Drawing.Size(58, 17);
            this.ProxiesLabel.TabIndex = 2;
            this.ProxiesLabel.Text = "Proxies:";
            // 
            // UsernameField
            // 
            this.UsernameField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsernameField.Location = new System.Drawing.Point(177, 422);
            this.UsernameField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.UsernameField.Name = "UsernameField";
            this.UsernameField.Size = new System.Drawing.Size(209, 22);
            this.UsernameField.TabIndex = 4;
            // 
            // ProxiesField
            // 
            this.ProxiesField.FormattingEnabled = true;
            this.ProxiesField.IntegralHeight = false;
            this.ProxiesField.ItemHeight = 16;
            this.ProxiesField.Location = new System.Drawing.Point(489, 384);
            this.ProxiesField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ProxiesField.Name = "ProxiesField";
            this.ProxiesField.Size = new System.Drawing.Size(512, 62);
            this.ProxiesField.TabIndex = 9;
            // 
            // MessageLabel
            // 
            this.MessageLabel.AutoSize = true;
            this.MessageLabel.Location = new System.Drawing.Point(28, 464);
            this.MessageLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MessageLabel.Name = "MessageLabel";
            this.MessageLabel.Size = new System.Drawing.Size(69, 17);
            this.MessageLabel.TabIndex = 2;
            this.MessageLabel.Text = "Message:";
            // 
            // MessageField
            // 
            this.MessageField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MessageField.Location = new System.Drawing.Point(177, 460);
            this.MessageField.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MessageField.Name = "MessageField";
            this.MessageField.Size = new System.Drawing.Size(824, 22);
            this.MessageField.TabIndex = 4;
            // 
            // DotNetCasProxyDemoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 502);
            this.Controls.Add(this.ProxiesField);
            this.Controls.Add(this.StatusField);
            this.Controls.Add(this.StatusColorField);
            this.Controls.Add(this.ServerResponseField);
            this.Controls.Add(this.ProxyValidateUrl);
            this.Controls.Add(this.MessageField);
            this.Controls.Add(this.UsernameField);
            this.Controls.Add(this.ValidationUrlField);
            this.Controls.Add(this.ProxyTicketField);
            this.Controls.Add(this.TargetServiceNameField);
            this.Controls.Add(this.ProxiesLabel);
            this.Controls.Add(this.MessageLabel);
            this.Controls.Add(this.UsernameLabel);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.ValidationUrlLabel);
            this.Controls.Add(this.ProxyTicketLabel);
            this.Controls.Add(this.TargetServiceNameLabel);
            this.Controls.Add(this.ProxyValidateUrlLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DotNetCasProxyDemoForm";
            this.ShowIcon = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CAS Proxy Ticket ClickOnce Demo";
            this.StatusColorField.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ProxyValidateUrlLabel;
        private System.Windows.Forms.Label TargetServiceNameLabel;
        private System.Windows.Forms.Label ProxyTicketLabel;
        private System.Windows.Forms.TextBox TargetServiceNameField;
        private System.Windows.Forms.TextBox ProxyTicketField;
        private System.Windows.Forms.TextBox ProxyValidateUrl;
        private System.Windows.Forms.TextBox ServerResponseField;
        private System.Windows.Forms.Label ValidationUrlLabel;
        private System.Windows.Forms.TextBox ValidationUrlField;
        private System.Windows.Forms.Panel StatusColorField;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.Label StatusField;
        private System.Windows.Forms.Label UsernameLabel;
        private System.Windows.Forms.Label ProxiesLabel;
        private System.Windows.Forms.TextBox UsernameField;
        private System.Windows.Forms.ListBox ProxiesField;
        private System.Windows.Forms.Label StatusExceptionField;
        private System.Windows.Forms.Label MessageLabel;
        private System.Windows.Forms.TextBox MessageField;
    }
}

